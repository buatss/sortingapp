package com.epam.SortingApp;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingTest extends TestCase {

    private int[] actual;
    private int[] expected;
    Sorting sorting = new Sorting();

    public SortingTest (int[] actual, int[] expected){
        this.actual = actual;
        this.expected = expected;
    }

    @Test (expected = IllegalArgumentException.class)
    public void testAscendingSortNullCase() {
        sorting.sortAscending(null);
    }

    @Test
    public void testAscendingSortEmptyCase(){
        int[] actual = {};
        int[] expected = {};
        sorting.sortAscending(actual);
        assertArrayEquals(expected,actual);
    }

    @Test
    public void testAscendingSortSingleCase(){
        int[] actual = {7};
        int[] expected = {7};
        sorting.sortAscending(actual);
        assertArrayEquals(expected,actual);
    }

    @Test
    public void testAscendingSortSortedArrayCase(){
        assertArrayEquals(expected,sorting.sortAscending(actual));
    }
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return
                Arrays.asList(new Object[][]{
                        {new int[]{3,2,1},new int[]{1,2,3}},
                        {new int[]{14,107,-18},new int[]{-18,14,107}},
                        {new int[]{-5,-17,-21},new int[]{-21,-17,-5}},
                        {new int[]{2,3,2},new int[]{2,2,3}}
                });
    }
}