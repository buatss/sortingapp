package com.epam.SortingApp;

import java.util.Arrays;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int[] input = new int[10];
        System.out.println("Welcome to sorting app, please enter up to 10 integers, to stop earlier enter non-number character");
        int i = 0;
        while (scanner.hasNextInt() && i < 10) {
            input[i] = scanner.nextInt();
            i++;
        }
        int[] numbers = new int[i];
        System.arraycopy(input, 0, numbers, 0, i);
        Sorting.sortAscending(numbers);

        System.out.println("You entered: " + Arrays.toString(numbers));
        System.out.println("Sorted: " + Arrays.toString(numbers));
    }
}
