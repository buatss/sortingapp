package com.epam.SortingApp;

import java.util.Arrays;

public class Sorting {

    public static int[] sortAscending (int[] input) throws IllegalArgumentException{
        if (input == null){
            throw new IllegalArgumentException();
        }
        Arrays.sort(input);
        return input;
    }
}
